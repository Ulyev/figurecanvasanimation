FigureCanvasAnimation
=====================
Jquery plugin to animate the substrate on the basis of the canvas.

![picture alt](https://bitbucket.org/Ulyev/figurecanvasanimation/raw/4bdc1468d5fdbd81408b75f144fd725664d49cfe/example_img/leftCenter.gif)
![picture alt](https://bitbucket.org/Ulyev/figurecanvasanimation/raw/4bdc1468d5fdbd81408b75f144fd725664d49cfe/example_img/rightCenterRandom.gif)
![picture alt](https://bitbucket.org/Ulyev/figurecanvasanimation/raw/4bdc1468d5fdbd81408b75f144fd725664d49cfe/example_img/leftBottom.gif)
![picture alt](https://bitbucket.org/Ulyev/figurecanvasanimation/raw/4bdc1468d5fdbd81408b75f144fd725664d49cfe/example_img/leftBottomRandom.gif)
![picture alt](https://bitbucket.org/Ulyev/figurecanvasanimation/raw/4bdc1468d5fdbd81408b75f144fd725664d49cfe/example_img/full.gif)
![picture alt](https://bitbucket.org/Ulyev/figurecanvasanimation/raw/4bdc1468d5fdbd81408b75f144fd725664d49cfe/example_img/fullCircle.gif)
![picture alt](https://bytebucket.org/Ulyev/figurecanvasanimation/raw/011aea26f6a22773369cb2eed0e4deac9d391568/example_img/mainLine.gif)
![picture alt](https://bytebucket.org/Ulyev/figurecanvasanimation/raw/011aea26f6a22773369cb2eed0e4deac9d391568/example_img/emptyCircle.gif)

##Connection
In header:
```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<script src="js/figureCanvasAnimation-1.0.js"></script>
```

In body:
```javascript
$(window).load(function(){
    $('#canvas').figureCanvasAnimation({
        direction: 'rightCenter',
        colors: [
            "0, 159, 255",
            "0, 54, 255"
        ],
        size: 100,
    });
});
```

## Options
| Attribute | Type | Default | Description | 
| :---: | :---: | :---: | :--- |
| `direction` | String | `rightCenter` | type of dice position on the screen.
| `corner` | Integer | `45` | The angle of the cubes on the screen (not working for `fullCircle` and `emptyCircle`).
| `opacity` | Float | `0.2` | Total transparency of cubes.
| `animation_from` | String | `right` | Animation direction.
| `random` | Boolean | `False` | Random arrangement of elements on one axis.
| `random2d` | Boolean | `False` | Random arrangement of elements along two axes (Does not work if `random` is `False`).
| `delay` | Float | `0.2` | Animation delay.
| `size` | Integer | `90` | The size of the dice.
| `padding` | Integer | `0` | Indent the entire group of cubes.
| `noise` | Integer | `0` | Add noise cube groups equal to the current number of cubes (x*noise).
| `different_size` | Integer | `0` | Using cubes of different sizes (from `size` to `different_size`).
| `different_opacity` | Integer | `0` | The use of different transparency of the cubes (from `opacity` to `different_opacity`).
| `colors` | Array | `["0, 159, 255"]` | Rgb color array.
| `circle.radius_circle` | Integer | `250` | Circle size in pixels.
| `circle.center_circle.x` | Integer | `window.innerWidth/2` | x-coordinate of the circle center.
| `circle.center_circle.y` | Integer | `window.innerHeight/2` | y-coordinate of the circle center.
| `line.size` | Integer | `25` | The size of the line (depending on the size of the screen).

## Attributes
**- direction**  


    
*Type of dice position on the screen.*  

*  `full` - Cubes are placed on the whole screen.
*  `leftBottom` - Cubes are located at the bottom left.
*  `rightTop` - Cubes are located at the top right.
*  `leftTop` - Cubes are located at the top left.
*  `rightBottom` - Cubes are located at the bottom right.
*  `fullCircle` - Cubes are arranged in a circle with circle `coordinates.center_circle.x` and `circle.center_circle.y`, with the radius of the `circle.radius_circle`.
*  `emptyCircle` - Cubes are arranged in a circle with a void inside. The void is controlled by the `padding` parameter.
*  `mainLine` - Cubes are located on the main diagonal. The line width is set to `line.size`. The value depends on the size of the screen!
*  `sideLine` - Cubes are placed on the secondary diagonal. The line width is set to `line.size`. The value depends on the size of the screen!
*  `leftCenter` - Cubes are located on the left in the center corner.
*  `rightCenter` - Cubes are located on the right in the center corner.
---
**- corner**  

*The angle of the cubes on the screen (not working for `fullCircle` and `emptyCircle`).*  

The angle is set in degrees. It is recommended to use values between 15 and 80 degrees.

---

**- opacity** 

*Total transparency of cubes.*  

It is recommended to use values from 0.1 to 0.9.  
If `different_opacity` is not `0`, then `opacity` and `different_opacity` are the boundary of the interval.
---
**- animation_from**  

*Animation direction.*

*  `right` - Cubes will fly to the right.
*  `left` - Cubes will fly to the left.
*  `random` - Cubes will fly and the right and left randomly.
---
**- size**  

*The size of the dice.*

If 'different_size `is not 0,` size `and` different_size' serve as the interval boundary.  
Please note that the number of cubes is calculated from the value of `size`. **Thus, if `different_size`<`size`, then a dice will be less than `different_size`>`size` with the same values of the variables.**
---
**- padding**  

*Indent the entire group of cubes.*  

With this option you can move sets of cubes. Both positive and negative values are accepted.  
When `direction` = `emptyCircle`, the parameter is used to define the inner indent of the circle. In this case `padding` >= `0`.
---
**- noise**  

*Add noise cube groups equal to the current number of cubes.*  

The number of cubes in the sample is multiplied by `noise`. Be careful with this option, **do not overload the browser!**

---
**- different_size**  

*Using cubes of different sizes (from `size` to `different_size`).* 

If you do not want to use different parameters, set the value to `0`.  
**If `different_size`<`size`, then a dice will be less than `different_size`>`size` with the same values of the variables.**
---
**- colors**  

*Rgb color array.* 

An array of strings containing rgb colors.  
Note that the brackets should not be written: `"0, 159, 255"`.  
Can contain any number of colors.
---
**- circle**  

*Cubes become in the form of a circle.* 

Works with `direction` = `fullCircle` or `direction` = `empty Circle`.
*  `radius_circle` - Circle size in pixels. The larger the radius and smaller the `size`, the greater the load on the browser.
*  `center_circle` - Сoordinates of the center of the circle
  * `x` - X-coordinate of the circle center.
  * `y` - Y-coordinate of the circle center.
    
## Example
**Below is only a small part of the options. It all depends on your imagination!**  
  
Left center:  
![picture alt](https://bitbucket.org/Ulyev/figurecanvasanimation/raw/4bdc1468d5fdbd81408b75f144fd725664d49cfe/example_img/leftCenter.gif)
```javascript
$(window).load(function(){
    $('#canvas').figureCanvasAnimation({
        direction: 'leftCenter',
        random: false,
        random2d: false,
        colors: [
            "0, 159, 255",
        ],
        size: 120,
        opacity: 0.3,
        different_size: 0,
        different_opacity: false,
        padding: 0,
        line: {
            size: -100
        }
    });
});
```
---
Right center random:  
![picture alt](https://bitbucket.org/Ulyev/figurecanvasanimation/raw/4bdc1468d5fdbd81408b75f144fd725664d49cfe/example_img/rightCenterRandom.gif)
```javascript
$(window).load(function(){
    $('#canvas').figureCanvasAnimation({
        direction: 'rightCenter',
        random: true,
        random2d: true,
        colors: [
            "0, 159, 255",
            "0, 54, 255",
            "0, 255, 222"
        ],
        size: 100,
        opacity: 0.3,
        different_size: 20,
        different_opacity: 0.6
    });
});
```
---
Left bottom:  
![picture alt](https://bitbucket.org/Ulyev/figurecanvasanimation/raw/4bdc1468d5fdbd81408b75f144fd725664d49cfe/example_img/leftBottom.gif)
```javascript
$(window).load(function(){
    $('#canvas').figureCanvasAnimation({
        direction: 'leftBottom',
        random: false,
        random2d: false,
        colors: [
            "0, 159, 255",
            "0, 54, 255",
            "0, 255, 222"
        ],
        size: 100,
        opacity: 0.3,
        different_size: false,
        different_opacity: false,
        padding: -100,
    });
});
```
---
Left bottom random:  
![picture alt](https://bitbucket.org/Ulyev/figurecanvasanimation/raw/4bdc1468d5fdbd81408b75f144fd725664d49cfe/example_img/leftBottomRandom.gif)
```javascript
$(window).load(function(){
    $('#canvas').figureCanvasAnimation({
        direction: 'leftBottom',
        random: true,
        random2d: true,
        colors: [
            "0, 159, 255",
            "0, 54, 255",
            "0, 255, 222"
        ],
        size: 100,
        opacity: 0.3,
        different_size: 40,
        different_opacity: 0.7,
        padding: -100
    });
});
```
---
Full:  
![picture alt](https://bitbucket.org/Ulyev/figurecanvasanimation/raw/4bdc1468d5fdbd81408b75f144fd725664d49cfe/example_img/full.gif)
```javascript
$(window).load(function(){
    $('#canvas').figureCanvasAnimation({
        direction: 'full',
        random: false,
        random2d: false,
        colors: [
            "0, 159, 255",
            "0, 54, 255",
            "0, 255, 222"
        ],
        size: 120,
        opacity: 0.3,
        different_size: false,
        different_opacity: false
    });
});
```
---
Full circle:  
![picture alt](https://bitbucket.org/Ulyev/figurecanvasanimation/raw/4bdc1468d5fdbd81408b75f144fd725664d49cfe/example_img/fullCircle.gif)
```javascript
$(window).load(function(){
    $('#canvas').figureCanvasAnimation({
        direction: 'fullCircle',
        random: false,
        random2d: false,
        colors: [
            "0, 159, 255",
            "255, 0, 0"
            // "0, 54, 255",
            // "0, 255, 222"
        ],
        size: 20,
        opacity: 0.3,
        different_size: false,
        different_opacity: false,
        padding: 0
    });
});
```
---
Empty circle:  
![picture alt](https://bytebucket.org/Ulyev/figurecanvasanimation/raw/011aea26f6a22773369cb2eed0e4deac9d391568/example_img/emptyCircle.gif)
```javascript
$(window).load(function(){
    $('#canvas').figureCanvasAnimation({
        direction: 'emptyCircle',
        random: false,
        random2d: false,
        colors: [
            "0, 159, 255",
            "255, 0, 0"
        ],
        size: 20,
        opacity: 0.3,
        different_size: false,
        different_opacity: false,
        padding: 40
    });
});
```
---
Main line:  
![picture alt](https://bytebucket.org/Ulyev/figurecanvasanimation/raw/011aea26f6a22773369cb2eed0e4deac9d391568/example_img/mainLine.gif)
```javascript
$(window).load(function(){
    $('#canvas').figureCanvasAnimation({
        direction: 'mainLine',
        random: false,
        random2d: false,
        colors: [
            "0, 159, 255",
            "255, 0, 0"
        ],
        size: 80,
        opacity: 0.3,
        different_size: false,
        different_opacity: false,
        padding: 0
    });
});
```