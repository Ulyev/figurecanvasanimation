(function($){
    jQuery.fn.figureCanvasAnimation = function(options){
        options = $.extend({
            direction: "rightCenter",
            corner: 45, //угол
            opacity: 0.2,
            animation_from: 'right',
            random: false,
            random2d: false,
            delay: 0.2,
            size: 90,
            padding: 0,
            noise: 0,
            different_size: false,
            different_opacity: false,
            colors: [
                "0, 159, 255"
            ],
            circle: {
                radius_circle: 250,
                center_circle: {
                    x: window.innerWidth/2,
                    y: window.innerHeight/2
                }
            },
            line: {
                size: 25
            }
        }, options);

        var make = function(){
            options.canvas = $(this);
            options.ctx = options.canvas[0].getContext("2d");
            options.ctx.canvas.width = window.innerWidth;
            options.ctx.canvas.height = window.innerHeight;
            options.particles = [];
            animation();
        };

        function drawDiamond(context, x, y, width, height, color, opacity){
            context.save();
            context.beginPath();
            context.moveTo(x, y);
            context.lineTo(x - width / 2, y + height / 2);
            context.lineTo(x, y + height);
            context.lineTo(x + width / 2, y + height / 2);
            context.closePath();
            context.fillStyle = "rgba(" + color + ", " + opacity + ")";
            console.log ("rgba(" + color + ", " + opacity + ")");
            context.shadowColor = 'rgba(0, 0, 0, 0.5)';
            context.shadowBlur = 25;
            context.shadowOffsetY = 8;
            context.shadowOffsetX = -8;
            context.fill();
            context.restore();
        }

        function randomInteger(min, max) {
            var rand = min + Math.random() * (max - min);
            rand = Math.round(rand);
            return rand;
        }

        function setAnimation(x, y, color, noise, current_animation){
            let random = 0;
            let random2 = 0;
            if (options.random || noise){
                random = randomInteger(5,50);
                random2 = randomInteger(5,50);

                if (!options.random2d){
                    random2 = random;
                }
            }

            let different_size = 0;
            if (options.different_size){
                different_size = randomInteger(options.size, options.different_size);
            }

            let different_opacity = options.opacity;
            if (options.different_opacity){
                different_opacity = randomInteger(options.opacity, options.different_opacity * 10)/10;
            }

            var particle = {
                x0: x-random,
                y0: y-random2,
                x1: current_animation.x,
                y1: current_animation.y,
                speed: Math.random() * 4 + 2,
                color: color,
                size: options.size + different_size,
                opacity: different_opacity
            };
            TweenMax.to(particle, particle.speed, {
                x1: particle.x0,
                y1: particle.y0,
                delay: options.delay,
                ease: Elastic.easeOut
            });
            options.particles.push(particle);
        }

        var render = function() {
            options.ctx.clearRect(0, 0, options.ctx.canvas.width, options.ctx.canvas.height);
            let different_size = 0;
            for (var i = 0, j = options.particles.length; i < j; i++) {
                if (options.different_size){
                    different_size = randomInteger(0, options.different_size);
                }
                var particle = options.particles[i];
                drawDiamond(options.ctx, particle.x1 * 3, particle.y1 * 3, particle.size, particle.size,
                    particle.color, particle.opacity);
            }
            requestAnimationFrame(render);
        };

        function degToRad(deg) {
            return deg / 180 * Math.PI;
        }

        function chooseMethod(y, x, current_color, noise, current_animation){
            switch(options.direction){
                case 'full':
                    setAnimation(x, y, options.colors[current_color], noise, current_animation);
                    break;
                case 'leftBottom':
                    leftBottom(y, x, current_color, noise, current_animation);
                    break;
                case 'rightTop':
                    rightTop(y, x, current_color, noise, current_animation);
                    break;
                case 'leftTop':
                    leftTop(y, x, current_color, noise, current_animation);
                    break;
                case 'rightBottom':
                    rightBottom(y, x, current_color, noise, current_animation);
                    break;
                case 'fullCircle':
                    fullCircle(y, x, current_color, noise, current_animation);
                    break;
                case 'emptyCircle':
                    emptyCircle(y, x, current_color, noise, current_animation);
                    break;
                case 'mainLine':
                    mainLine(y, x, current_color, noise, current_animation);
                    break;
                case 'sideLine':
                    sideLine(y, x, current_color, noise, current_animation);
                    break;
                case 'leftCenter':
                    leftCenter(y, x, current_color, noise, current_animation);
                    break;
                case 'rightCenter':
                    rightCenter(y, x, current_color, noise, current_animation);
                    break;
            }
        }

        function leftBottom(y, x, current_color, noise, current_animation){
            let y_line = x*Math.tan(degToRad(options.corner)) -
                options.padding/3;
            if (options.ctx.canvas.width < options.ctx.canvas.height){
                y_line += Math.abs(options.ctx.canvas.width-options.ctx.canvas.height)/3;
            }
            if (y - y_line > -1) {
                setAnimation(x, y, options.colors[current_color], noise, current_animation);
            }
        }

        function rightTop(y, x, current_color, noise, current_animation){
            let y_line = x*Math.tan(degToRad(options.corner)) -
                options.padding/3;
            if (options.ctx.canvas.width > options.ctx.canvas.height){
                y_line -= Math.abs(options.ctx.canvas.width-options.ctx.canvas.height)/3;
            }
            if (y_line - y > 1) {
                setAnimation(x, y, options.colors[current_color], noise, current_animation);
            }
        }

        function leftTop(y, x, current_color, noise, current_animation){
            let y_line = x*Math.tan(degToRad(180-options.corner)) -
                options.padding/3;
            y_line += Math.min(options.ctx.canvas.height, options.ctx.canvas.width)/3;
            if (Math.floor(y_line) >= Math.floor(y)) {
                setAnimation(x, y, options.colors[current_color], noise, current_animation);
            }
        }

        function rightBottom(y, x, current_color, noise, current_animation){
            let y_line = x*Math.tan(degToRad(180-options.corner)) -
                options.padding/3;
            y_line += Math.max(options.ctx.canvas.height, options.ctx.canvas.width)/3;
            if (Math.floor(y_line) - Math.floor(y) <=1 ) {
                setAnimation(x, y, options.colors[current_color], noise, current_animation);
            }
        }

        function fullCircle(y, x, current_color, noise, current_animation){
            let condition1 = Math.pow(x-options.circle.center_circle.x/3,2) +
                Math.pow(y-options.circle.center_circle.y/3, 2);
            if (Math.pow(options.circle.radius_circle/3,2) >= condition1){
                setAnimation(x, y, options.colors[current_color], noise, current_animation);
            }
        }

        function emptyCircle(y, x, current_color, noise, current_animation){
            let main_circle = Math.pow(x-options.circle.center_circle.x/3,2) +
                Math.pow(y-options.circle.center_circle.y/3, 2);
            if (Math.pow(options.circle.radius_circle/3,2) >= main_circle &&
                Math.pow((options.circle.radius_circle-options.padding)/3,2) < main_circle){
                setAnimation(x, y, options.colors[current_color], noise, current_animation);
            }
        }

        function mainLine(y, x, current_color, noise, current_animation){
            let y_line1 = x*Math.tan(degToRad(options.corner)) + options.line.size/3
                - options.padding/3;
            let y_line2 = x*Math.tan(degToRad(options.corner)) - options.line.size/3
                - Math.abs(options.ctx.canvas.width-options.ctx.canvas.height)/3
                - options.padding/3;
            if (Math.floor(y_line1) > Math.floor(y) && Math.floor(y_line2) < Math.floor(y)) {
                setAnimation(x, y, options.colors[current_color], noise, current_animation);
            }
        }

        function sideLine(y, x, current_color, noise, current_animation){
            let y_line1 = x*Math.tan(degToRad(180-options.corner)) - options.line.size/3
                - options.padding/3
                + Math.min(options.ctx.canvas.height, options.ctx.canvas.width)/3;

            let y_line2 = x*Math.tan(degToRad(180-options.corner)) - options.line.size/3
                - options.padding/3
                + Math.max(options.ctx.canvas.height, options.ctx.canvas.width)/3;

            if (Math.floor(y_line1) < Math.floor(y) && Math.floor(y_line2) > Math.floor(y)) {
                setAnimation(x, y, options.colors[current_color], noise, current_animation);
            }
        }

        function leftCenter(y, x, current_color, noise, current_animation){
            let y_line1 = x*Math.tan(degToRad(options.corner)) -
                options.padding/3;

            let y_line2 = x*Math.tan(degToRad(180-options.corner)) -
                options.padding/3;
            y_line2 += Math.min(options.ctx.canvas.height, options.ctx.canvas.width)/3;

            if (Math.floor(y_line1) <= Math.floor(y) && Math.floor(y_line2) - Math.floor(y) >1){
                setAnimation(x, y, options.colors[current_color], noise, current_animation);
            }
        }

        function rightCenter(y, x, current_color, noise, current_animation){
            let y_line1 = x*Math.tan(degToRad(options.corner)) -
                options.padding/3;
            y_line1 -= Math.abs(options.ctx.canvas.width-options.ctx.canvas.height)/3;

            let y_line2 = x*Math.tan(degToRad(180-options.corner)) -
                options.padding/3;
            y_line2 += Math.max(options.ctx.canvas.height, options.ctx.canvas.width)/3;
            if (Math.floor(y_line1) - Math.floor(y) > 1 && Math.floor(y_line2) - Math.floor(y)<0) {
                setAnimation(x, y, options.colors[current_color], noise, current_animation);
            }
        }

        function animation() {
            let list_animation_from = {
                'left': {
                    x: -options.ctx.canvas.width,
                    y: -options.ctx.canvas.height
                },
                'right': {
                    x: options.ctx.canvas.width,
                    y: options.ctx.canvas.height
                }
            };
            let animation_from = false;
            switch (options.animation_from) {
                case 'left':
                    animation_from = list_animation_from.left;
                    break;
                case 'right':
                    animation_from = list_animation_from.right;
                    break;
                case 'random':
                    animation_from = false;
                    break;
            }

            let current_color = 0;
            let dop_element = 0;
            if (options.random){
                dop_element = options.size/randomInteger(12, 15);
            }

            let current_animation = animation_from;

            for (let x = 0; x < options.ctx.canvas.width; x += options.size/3 - dop_element){
                for (let y = 0; y < options.ctx.canvas.height; y += options.size/3 - dop_element){

                    if (animation_from === false){
                        if (randomInteger(0, 1) > 0){
                            current_animation = list_animation_from.left;
                        } else {
                            current_animation = list_animation_from.right;
                        }
                    }

                    chooseMethod(y, x, current_color, false, current_animation);
                    for (let i = 0; i < options.noise; i++) {
                        chooseMethod(y, x, current_color, true, current_animation);
                    }

                    current_color++;
                    if(current_color>=options.colors.length){
                        current_color = 0;
                    }
                }
            }
            requestAnimationFrame(render);
        }

        return this.each(make);
    };
})(jQuery);